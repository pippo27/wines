//
//  DetailViewController.swift
//  Wines
//
//  Created by Arthit Thongpan on 1/28/2558 BE.
//  Copyright (c) 2558 Arthit Thongpan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var toolbar: UIToolbar!
    
    @IBOutlet weak var dateButton: UIBarButtonItem!
    
    var tutorialsButtonItem : UIBarButtonItem!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var grapes: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var region: UILabel!
    @IBOutlet weak var year: UILabel!
    
    @IBOutlet weak var descriptionLabel: UITextView!
    var wine:Wine!
    var imageCache:[String: UIImage] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        toolbar.hidden = true
        tutorialsButtonItem = UIBarButtonItem(title: "Wines", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(showTutorialsViewController(_:)))
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(handleFirstViewControllerDisplayModeChangeWithNotification(_:)), name: "PrimaryVCDisplayModeChangeNotification", object: nil)
        
        clearText()
        showDetail()
        
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func showTutorialsViewController(sender:UIBarButtonItem){
        print(sender)
        splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
    }
    func clearText(){
        name.text = ""
        grapes.text = ""
        country.text = ""
        region.text = ""
        year.text = ""
        
        descriptionLabel.text = ""

    }
    func showDetail(){
        if wine != nil {
            
            name.text = "Name:\(wine.name)"
            grapes.text = "Grapes: \(wine.grapes)"
            country.text = "Country: \(wine.country)"
            region.text = "Region: \(wine.region)"
            year.text = "Year: \(wine.year)"
            descriptionLabel.text = "Note: \(wine.description)"
            
            var image:UIImage? = self.imageCache[wine.imageURL]
            
            if image == nil && (wine.imageURL != "") {
                
                let request:NSURLRequest = NSURLRequest(URL: NSURL(string: Constants.URL.IMAGE_URL + self.wine.imageURL)!)
                let session = NSURLSession.sharedSession()
                
                let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                    if error == nil {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                           // self.image = image
                            
                            image = UIImage(data: data!)
                            self.imageCache[self.wine.imageURL] = image
                            
                            self.imageView.image = image
                            self.view.setNeedsLayout()
                        })
                        
                    }else {
                        print("Error: \(error!.localizedDescription)")
                    }
                })
                task.resume()
            }
            
    
            if toolbar.hidden {
                toolbar.hidden = false
                
            }
            
            if self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.Compact{
                toolbar.items?.insert(self.splitViewController!.displayModeButtonItem(), atIndex: 0)
            }
        }
    }
    
    func handleFirstViewControllerDisplayModeChangeWithNotification(notification: NSNotification){
        let displayModeObject = notification.object as? NSNumber
        let nextDisplayMode = displayModeObject?.integerValue
        
        if toolbar.items?.count == 3{
            toolbar.items?.removeAtIndex(0)
        }
        
        if nextDisplayMode == UISplitViewControllerDisplayMode.PrimaryHidden.rawValue {
            toolbar.items?.insert(tutorialsButtonItem, atIndex: 0)
        }
        else{
            toolbar.items?.insert(splitViewController!.displayModeButtonItem(), atIndex: 0)
        }
    }
    
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        
        if previousTraitCollection?.verticalSizeClass == UIUserInterfaceSizeClass.Compact{
            let firstItem = toolbar.items?[0] as UIBarButtonItem!
            if firstItem?.title == "Wines"{
                toolbar.items?.removeAtIndex(0)
            }
        }
        else if previousTraitCollection?.verticalSizeClass == UIUserInterfaceSizeClass.Regular{
            if toolbar.items?.count == 3{
                toolbar.items?.removeAtIndex(0)
            }
            
            if splitViewController?.displayMode == UISplitViewControllerDisplayMode.PrimaryHidden {
                toolbar.items?.insert(tutorialsButtonItem, atIndex: 0)
            }
            else{
                toolbar.items?.insert(self.splitViewController!.displayModeButtonItem(), atIndex: 0)
            }
        }
    }

    @IBAction func showDate(sender: AnyObject) {
        
        let popoverViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("idPopoverViewController") as? PopoverViewController
        
        popoverViewController?.modalPresentationStyle = UIModalPresentationStyle.Popover
        popoverViewController?.popoverPresentationController?.delegate = self
        self.presentViewController(popoverViewController!, animated: true, completion: nil)
        
        popoverViewController?.popoverPresentationController?.barButtonItem = dateButton
        popoverViewController?.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Any
        popoverViewController?.preferredContentSize = CGSizeMake(200.0, 80.0)
        
        popoverViewController?.messageLabel.text = "Year"
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
}
