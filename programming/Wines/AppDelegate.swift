//
//  AppDelegate.swift
//  Wines
//
//  Created by Arthit Thongpan on 1/28/2558 BE.
//  Copyright (c) 2558 Arthit Thongpan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    var splitViewController : UISplitViewController?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
       
//        
//        let data:[String: AnyObject] = [:]  // ก่อน json  ที่ใจะใช้อ่าน ในที่นีพี่ชื่อ data
//        
//        struct Product {
//            var price:Double = 0.0
//            var dateStr:String?  // อันนี้จะเก็บเป็น String รึว่าจะไปแปลงเป็น NSDate ก่อนก็ได้
//            
//        }
//        
//        
//        
//        let dataArray:NSArray = data["data"] as! NSArray!
//        var productList:[Product] = []
//        for dic in dataArray {
//            let price = dic["prices"] as! Double
//            let date = dic["date"] as! String
//            let product = Product(price: price, dateStr: date)
//            productList.append(product) // productList  เป็น array ที่เก็บ Instance ของ Class Product
//        }
//        
//        
//        
//        
        
        /*
        let splitViewController = window.rootViewController as UISplitViewController
        splitViewController.delegate = self
        splitViewController.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
        
        let containerViewController:ContainerViewController = ContainerViewController()
        containerViewController.setEmbeddedViewController(splitViewController)
        window!.rootViewController = containerViewController
        
        */
        
        splitViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("idSplitViewController") as? UISplitViewController
        splitViewController?.delegate = self
        
        splitViewController?.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
        
        let containerViewController : ContainerViewController = ContainerViewController()
        containerViewController.setEmbeddedViewController(splitViewController)
        
        window!.rootViewController = containerViewController
        
       
                
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func targetDisplayModeForActionInSplitViewController(svc: UISplitViewController) -> UISplitViewControllerDisplayMode {
        return UISplitViewControllerDisplayMode.PrimaryHidden
    }

    func splitViewController(svc: UISplitViewController, willChangeToDisplayMode displayMode: UISplitViewControllerDisplayMode) {
        NSNotificationCenter.defaultCenter().postNotificationName("PrimaryVCDisplayModeChangeNotification", object: displayMode.rawValue)
    }
}

