//
//  Wine.swift
//  Wines
//
//  Created by Arthit Thongpan on 1/28/2558 BE.
//  Copyright (c) 2558 Arthit Thongpan. All rights reserved.
//

import UIKit

class Wine{
    var id:String = "0"
    var name:String = ""
    var grapes:String = ""
    var country:String = ""
    var region:String = ""
    var year:String = ""
    var imageURL:String = ""
    var description:String = ""
    
    init(id:String, name:String, grapes:String, country:String, region:String, year:String, imageURL:String, description:String){
        
        self.id = id
        self.name = name
        self.grapes = grapes
        self.country = country
        self.region = region
        self.year = year
        self.imageURL = imageURL
        self.description = description
    }
   
}
