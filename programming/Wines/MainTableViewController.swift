//
//  MainTableViewController.swift
//  Wines
//
//  Created by Arthit Thongpan on 1/28/2558 BE.
//  Copyright (c) 2558 Arthit Thongpan. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {

    var wines:[Wine] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        loadData()
      
    }
    func loadData(){
        
        let dataManager = DataManager()
        dataManager.loadData(Constants.URL.GET_WINES, httpMethod: HttpMethod.GET, params: nil) { (data, response, error) in
            if error == nil {
                /*
                if let jsonWines: AnyObject = data["wine"]{
                println(jsonWines[])
                }
                */
                let dataArray:NSArray = data["wine"] as! NSArray!
                print(dataArray.count)
                
                for arr in dataArray {
                    print(arr)
                    
                    
                    let id = arr["id"] as! String
                    
                    let name = arr["name"]as! String
                    var picture = ""
                    if let pic: String = arr["picture"] as? String{
                        picture = pic
                    }
                    
                    var region = ""
                    if let value: String = arr["region"] as? String{
                        region = value
                    }
                    var year = ""
                    if let value: String = arr["year"] as? String{
                        year = value
                    }
                    
                    var country = ""
                    if let value: String = arr["country"] as? String{
                        country = value
                    }
                    
                    var description = ""
                    if let value: String = arr["description"] as? String{
                        description = value
                    }
                    
                    var grapes = ""
                    if let value: String = arr["grapes"] as? String{
                        grapes = value
                    }
                    
                    
                    print(arr)
                    let wine = Wine(id: id, name: name, grapes: grapes, country: country, region: region, year: year, imageURL: picture, description: description)
                    
                    self.wines.append(wine)
                }
                
            }
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return wines.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CustomCell", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...
        cell.textLabel!.text = "\(wines[indexPath.row].name)"

        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        let wine:Wine = wines[indexPath.row]
        let detailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("idDetailViewController") as! DetailViewController
        detailViewController.wine = wine
        
        showDetailViewController(detailViewController, sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        let cell = sender as UITableViewCell
        let indexPath = self.tableView.indexPathForCell(cell)!
        let wine:Wine = wines[indexPath.row]
        let detailViewController = segue.destinationViewController as DetailViewController
        detailViewController.wine = wine
        
    }
*/

}
