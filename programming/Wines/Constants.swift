//
//  Constants.swift
//  Wines
//
//  Created by Arthit Thongpan on 2/8/2558 BE.
//  Copyright (c) 2558 Arthit Thongpan. All rights reserved.
//

import Foundation


struct Constants {
    struct URL {
        static let ROOT_URL = "http://localhost/wines"
        static let IMAGE_URL = ROOT_URL + "/cellar/pics/"
        static let GET_WINES =  "\(ROOT_URL)/cellar/api/wines"
        //static let SEARCH_WINE =  ROOT_URL + "/cellar/api/wines"
        //static let ADD_WINE =  ROOT_URL + "/cellar/api/wines"
        
        
        
    }
}
